#include <OneWire.h>
#include <DallasTemperature.h>

#include <SPI.h>
#include <WiFi.h>

#define LED 2

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 4

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

float temp = 0;

char ssid[] = "TODEFINE"; // your network SSID (name)
char pass[] = "TODEFINE";       // your network password

int status = WL_IDLE_STATUS;

WiFiServer server(80);

// Variable to store the HTTP request
String header;

String ledState = "off";

void printWifiStatus()
{

  // print the SSID of the network you're attached to:

  Serial.print("SSID: ");

  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();

  Serial.print("IP Address: " + ip);
  // print the received signal strength:

  long rssi = WiFi.RSSI();

  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void setup(void)
{
  // start serial port
  Serial.begin(115200);
  // Start up the library
  sensors.begin();

  pinMode(LED, OUTPUT);

  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  // printWifiStatus();
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void turnOnLed()
{
  digitalWrite(LED, HIGH);
}

void turnOffLed()
{
  digitalWrite(LED, LOW);
}

void handleConnect(void)
{
  // listen for incoming clients
  WiFiClient client = server.available();

  if (client)
  {
    bool currentLineIsBlank = true;
    while (client.connected())
    {
      if (client.available())
      {
        char c = client.read();
        header += c;

        if (c == '\n' && currentLineIsBlank)
        {

          if (header.indexOf("GET /turn-on") >= 0)
          {
            ledState = "on";
            digitalWrite(LED, HIGH);
          }
          else if (header.indexOf("GET /turn-off") >= 0)
          {
            ledState = "off";
            digitalWrite(LED, LOW);
          }

          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close"); // the connection will be closed after completion of the response
          client.println("Refresh: 5");        // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          // add bootstrap
          client.println("<head>");
          client.println("<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC\" crossorigin=\"anonymous\">");
          client.println("</head>");
          client.println("<html>");
          String tempClass = "";
          if (temp > 27)
          {
            tempClass = "text-danger";
          }
          client.print("<p class=\"");
          client.print(tempClass);
          client.println("\">");
          client.println(temp);
          client.println(" &#176;C");
          client.println("</p>");
          if (ledState == "off")
          {
            client.println("<p><a href=\"/turn-on\"><button class=\"btn btn-primary\">ON</button></a></p>");
          }
          else
          {
            client.println("<p><a href=\"/turn-off\"><button class=\"btn btn-primary\">OFF</button></a></p>");
          }
          client.println("</html>");
          break;
        }

        if (c == '\n')
        {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }

    // give the web browser time to receive the data
    delay(1);

    // close the connection:
    header = "";
    client.stop();
  }
}

void loop(void)
{
  sensors.requestTemperatures(); // Send the command to get temperatures

  temp = sensors.getTempCByIndex(0);

  handleConnect();
}
